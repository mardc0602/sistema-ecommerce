﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EComemerce_01.Models;
using EComemerce_01.Classes;
using PagedList;

namespace EComemerce_01.Controllers
{
    [Authorize(Roles = "User")]
    public class SuppliersController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        // GET: Suppliers
        public ActionResult Index(int? page = null,string buscarCategoria = null)
        {
            page = (page ?? 1);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            var qry = (from cu in db.Suppliers
                       join cc in db.CompanySuppliers on cu.SupplierId equals cc.SupplierId
                       join co in db.Companies on cc.CompanyId equals co.CompanyId
                       where co.CompanyId == user.CompanyId
                       select new { cu }).ToList();

            var suppliers = new List<Supplier>();

            foreach (var item in qry)
            {
                suppliers.Add(item.cu);
            }

            if (!string.IsNullOrEmpty(buscarCategoria))
            {
                suppliers = suppliers.Where(s => s.FullName.ToUpper().Contains(buscarCategoria.ToUpper())
                       || s.Adress.ToUpper().Contains(buscarCategoria.ToUpper())
                       || s.Phone.ToUpper().Contains(buscarCategoria.ToUpper())
                       || s.City.Name.ToUpper().Contains(buscarCategoria.ToUpper())
                       || s.Department.Name.ToUpper().Contains(buscarCategoria.ToUpper())
                       || s.UserName.ToUpper().Contains(buscarCategoria.ToUpper())).ToList();

            }



            return View(suppliers.ToPagedList((int)page, 5));
        }

        // GET: Suppliers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        // GET: Suppliers/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(0), "CityId", "Name");
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name");
            return PartialView();
        }

        // POST: Suppliers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Suppliers.Add(supplier);
                        var response = DBHelper.SaveChanges(db);
                        if (!response.Succeeded)
                        {
                            ModelState.AddModelError(string.Empty, response.Message);
                            transaction.Rollback();
                            ViewBag.CityId = new SelectList(CombosHelper.GetCities(supplier.DepartmentId), "CityId", "Name", supplier.CityId);
                            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", supplier.DepartmentId);
                            return View(supplier);
                        }
                        UserHelper.CreateUserASP(supplier.UserName, "Supplier");
                        if (supplier.PhotoFile != null)
                        {

                            var folder = "~/Content/Suppliers";
                            var file = string.Format("{0}.jpg", supplier.SupplierId);
                            var resp= FilesHelper.UploadPhoto(supplier.PhotoFile, folder, file);
                            if (resp)
                            {
                                var pic = string.Format("{0}/{1}", folder, file);
                                supplier.Photo = pic;
                                db.Entry(supplier).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                        }

                        var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
                        var companySupplier = new CompanySupplier
                        {
                            CompanyId = user.CompanyId,
                            SupplierId = supplier.SupplierId
                        };

                        db.CompanySuppliers.Add(companySupplier);
                        db.SaveChanges();
                        transaction.Commit();
                        return Json(new { success = true });

                    }
                    catch (Exception ex)
                    { 
                    
                        transaction.Rollback();
                        ModelState.AddModelError(string.Empty, ex.Message);

                    }
                }
            }

            ViewBag.CityId = new SelectList(CombosHelper.GetCities(supplier.DepartmentId), "CityId", "Name", supplier.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", supplier.DepartmentId);
            return PartialView(supplier);
        }

        // GET: Suppliers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(supplier.DepartmentId), "CityId", "Name", supplier.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", supplier.DepartmentId);
            return PartialView(supplier);
        }

        // POST: Suppliers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                if (supplier.PhotoFile != null)
                {
                    var file = string.Format("{0}.jpg", supplier.SupplierId);
                    var folder = "~/Content/Suppliers";
                    var resp = FilesHelper.UploadPhoto(supplier.PhotoFile, folder, file);
                    supplier.Photo = string.Format("{0}/{1}", folder, file);    
                }

                db.Entry(supplier).State = EntityState.Modified;
                db.SaveChanges();
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {

                    return Json(new { success = true });
                }
                ModelState.AddModelError(string.Empty, response.Message);
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(supplier.DepartmentId), "CityId", "Name", supplier.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", supplier.DepartmentId);
            return PartialView(supplier);
        }

        // GET: Suppliers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return PartialView(supplier);
        }

        // POST: Suppliers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var supplier = db.Suppliers.Find(id);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var companySupplier = db.CompanySuppliers.Where(cc => cc.CompanyId == user.CompanyId && cc.SupplierId == supplier.SupplierId).FirstOrDefault();

            using (var transaction = db.Database.BeginTransaction())
            {
                db.CompanySuppliers.Remove(companySupplier);
                db.Suppliers.Remove(supplier);
                var response = DBHelper.SaveChanges(db);

                if (response.Succeeded)
                {
                    transaction.Commit();
                    return Json(new { success = true });
                }
                transaction.Rollback();
                ModelState.AddModelError(string.Empty, response.Message);
                return PartialView(supplier);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
