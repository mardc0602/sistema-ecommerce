﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EComemerce_01.Models;
using EComemerce_01.Classes;
using PagedList;

namespace EComemerce_01.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        // GET: Users
        public ActionResult Index(int? page = null, string buscarUser = null)
        {
            page = (page ?? 1);
            var users = db.User.Include(u => u.City).Include(u => u.Company).Include(u => u.Department).OrderBy(c => c.UserId);


            if (!string.IsNullOrEmpty(buscarUser))
            {
                var users2 = users.Where(s => s.FullName.ToUpper().Contains(buscarUser.ToUpper())
                            || s.City.Name.ToUpper().Contains(buscarUser.ToUpper())
                            || s.Department.Name.ToUpper().Contains(buscarUser.ToUpper())
                            || s.Adress.ToUpper().Contains(buscarUser.ToUpper())
                            || s.Company.Name.ToUpper().Contains(buscarUser.ToUpper())
                            || s.UserName.ToUpper().Contains(buscarUser.ToUpper())
                            || s.Phone.ToUpper().Contains(buscarUser.ToUpper())).ToList();

                return View(users2.ToPagedList((int)page, 5));

            }

            return View(users.ToPagedList((int)page, 5));
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(0), "CityId", "Name");
            ViewBag.CompanyId = new SelectList(CombosHelper.GetCompanies(), "CompanyId", "Name");
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name");
            return PartialView();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                db.User.Add(user);
                db.SaveChanges();
                UserHelper.CreateUserASP(user.UserName,"User");

                if (user.PhotoFile != null)
                {

                    var folder = "~/Content/Users";
                    var file = string.Format("{0}.jpg",user.UserId);
                    var response = FilesHelper.UploadPhoto(user.PhotoFile, folder, file);
                    if (response)
                    {
                        var pic = string.Format("{0}/{1}", folder, file);
                        user.Photo = pic;
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }

                return Json(new { success = true });

            }

            ViewBag.CityId = new SelectList(CombosHelper.GetCities(user.DepartmentId), "CityId", "Name", user.CityId);
            ViewBag.CompanyId = new SelectList(CombosHelper.GetCompanies(), "CompanyId", "Name", user.CompanyId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", user.DepartmentId);
            return PartialView(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(user.DepartmentId), "CityId", "Name", user.CityId);
            ViewBag.CompanyId = new SelectList(CombosHelper.GetCompanies(), "CompanyId", "Name", user.CompanyId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", user.DepartmentId);
            return PartialView(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                if (user.PhotoFile != null)
                {
                    var file = string.Format("{0}.jpg", user.UserId);
                    var folder = "~/Content/Users";
                    var response = FilesHelper.UploadPhoto(user.PhotoFile, folder, file);   
                    user.Photo =string.Format("{0}/{1}", folder, file);
                
                }
                var db2 = new ECommerceContext();
                var currentUser = db2.User.Find(user.UserId);
                if (currentUser.UserName!=user.UserName)
                {
                    UserHelper.UpdateUserName(currentUser.UserName, user.UserName);

                }
                db2.Dispose();
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true });
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(user.DepartmentId), "CityId", "Name", user.CityId);
            ViewBag.CompanyId = new SelectList(CombosHelper.GetCompanies(), "CompanyId", "Name", user.CompanyId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", user.DepartmentId);
            return PartialView(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return PartialView(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var user = db.User.Find(id);
            db.User.Remove(user);
            var response=DBHelper.SaveChanges(db);
            if (response.Succeeded)
            {
                UserHelper.DeleteUser(user.UserName,"User");
                return Json(new { success = true });
            }
            ModelState.AddModelError(string.Empty,response.Message);
            return PartialView(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
