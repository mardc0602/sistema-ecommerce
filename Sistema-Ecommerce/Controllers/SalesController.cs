﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EComemerce_01.Models;
using EComemerce_01.Classes;
using PagedList;
using System.Collections;

namespace EComemerce_01.Controllers
{
    [Authorize(Roles = "User")]
    public class SalesController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        [HttpPost]
        public ActionResult DeleteProduct2(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var SaleDetailTmp = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == id).FirstOrDefault();

            if (SaleDetailTmp == null)
            {
                return HttpNotFound();
            }
            db.SaleDetailTmps.Remove(SaleDetailTmp);
            db.SaveChanges();
            return Json(Url.Action("Create", "Sales"));
        }



        public ActionResult DeleteProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var SaleDetailTmp = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == id).FirstOrDefault();

            if (SaleDetailTmp == null)
            {
                return HttpNotFound();
            }
            db.SaleDetailTmps.Remove(SaleDetailTmp);
            db.SaveChanges();
            return RedirectToAction("Create");
        }

        [HttpPost]
        public ActionResult FromOrder(FromOrder view)
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                var SaleDetailTmp = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.OrderId == view.OrderId).FirstOrDefault();
                var order = db.OrderDetails.Where(odt => odt.OrderId == view.OrderId).ToList();

                foreach (var ordes in order)
                {
                    SaleDetailTmp = new SaleDetailTmp
                    {
                        Description = ordes.Description,
                        Price = ordes.Price,
                        ProductId = ordes.ProductId,
                        Quantity = ordes.Quantity,
                        TaxRate = ordes.TaxRate,
                        UserName = User.Identity.Name,
                        OrderId = ordes.OrderId

                    };

                    db.SaleDetailTmps.Add(SaleDetailTmp);
                }

                db.SaveChanges();
                return RedirectToAction("Create");
            }
            ViewBag.OrderId = new SelectList(CombosHelper.GetOrders(user.CompanyId, 1), "OrderId", "OrderId");
            return PartialView(view);

        }

        [HttpPost]
        public ActionResult Inventories(int[] ProductId, int war)
        {
            List<double> IdProductos = new List<double>();
            for (int i=0;i<ProductId.Length;i++) {

                int valor = ProductId[i];

                try
                {
                    var Inventories = db.Inventores.Where(c => c.ProductId == valor && c.WarehouseId == war).Select(c => c.Stock).First();

                    IdProductos.Add(Inventories);
                }
                catch (Exception)
                {
                    var Inventories = 0;
                    IdProductos.Add(Inventories);
                }
            }
            return Json(IdProductos);

        }



        public ActionResult FromOrder()
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.OrderId = new SelectList(CombosHelper.GetOrders(user.CompanyId,1), "OrderId", "OrderId");
            return PartialView();
        }


        public ActionResult Error()
        {
            return PartialView();
        }


        public ActionResult FromOrder2(int? page = null)
        {
            page = (page ?? 1);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var orders = db.Orders.Where(o => o.CompanyId == user.CompanyId && o.StateId==1).Include(o => o.Customer).Include(o => o.State).OrderBy(c => c.OrderId);
            return PartialView("FromOrder2",orders.ToPagedList((int)page, 5));

        }

        [HttpPost]
        public ActionResult FromOrder2(int? page = null, string search = null)
        {
            page = (page ?? 1);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var orders = db.Orders.Where(o => o.CompanyId == user.CompanyId && o.StateId == 1).Include(o => o.Customer).Include(o => o.State).OrderBy(c => c.OrderId);
            

            if (!string.IsNullOrEmpty(search))
            {
                var Orders2 = orders.Where(s => s.State.Description.ToUpper().Contains(search.ToUpper())
                       || s.Customer.FirstName.ToUpper().Contains(search.ToUpper())
                       || s.Customer.LastName.ToUpper().Contains(search.ToUpper())
                       || s.Remarks.ToUpper().Contains(search.ToUpper())).ToList();


                return PartialView("FromOrder2", Orders2.ToPagedList((int)page, 5));

            }

            return PartialView("FromOrder2", orders.ToPagedList((int)page, 5));
        }




        [HttpPost]
        public ActionResult FromOrderTolist(int id)
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                var SaleDetailTmp = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.OrderId == id).FirstOrDefault();
                var order = db.OrderDetails.Where(odt => odt.OrderId == id).ToList();

                foreach (var ordes in order)
                {
                    SaleDetailTmp = new SaleDetailTmp
                    {
                        Description = ordes.Description,
                        Price = ordes.Price,
                        ProductId = ordes.ProductId,
                        Quantity = ordes.Quantity,
                        TaxRate = ordes.TaxRate,
                        UserName = User.Identity.Name,
                        OrderId = ordes.OrderId

                    };

                    db.SaleDetailTmps.Add(SaleDetailTmp);
                }

                db.SaveChanges();
                return Json(Url.Action("Create", "Sales"));
            }
            
            return PartialView();

        }


        [HttpPost]
        public ActionResult AddProduct2(int? page = null, string search = null)
        {
            page = (page ?? 1);
            var user = db.User
                .Where(u => u.UserName == User.Identity.Name)
                .FirstOrDefault();
            var products = db.Products
                .Include(p => p.Category)
                .Include(p => p.Tax)
                .Where(p => p.CompanyId == user.CompanyId).OrderBy(c => c.ProductId);

            if (!string.IsNullOrEmpty(search))
            {
                var products2 = products.Where(s => s.Description.ToUpper().Contains(search.ToUpper())
                       || s.Tax.Description.ToUpper().Contains(search.ToUpper())
                       || s.Category.Description.ToUpper().Contains(search.ToUpper())).ToList();


                return PartialView("AddProduct2",products2.ToPagedList((int)page, 3));

            }

            return PartialView("AddProduct2",products.ToPagedList((int)page, 3));
        }


        // GET: Purchases
        public ActionResult AddProduct2(int? page = null)
        {
            page = (page ?? 1);
            var user = db.User
                .Where(u => u.UserName == User.Identity.Name)
                .FirstOrDefault();
            var products = db.Products
                .Include(p => p.Category)
                .Include(p => p.Tax)
                .Where(p => p.CompanyId == user.CompanyId).OrderBy(c => c.ProductId);

            return PartialView(products.ToPagedList((int)page, 3));


        }

        public ActionResult AddProductWarehouse(int? Warehouse, int? page = null)
        {
            page = (page ?? 1);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.WarehouseId = CombosHelper.GetWarehousers(user.CompanyId);
            ViewBag.IDBodega = Convert.ToInt32(Warehouse);
            List<Product> ListaProductWarehouse = CombosHelper.ProductWareHouse(Convert.ToInt32(Warehouse));

            return PartialView(ListaProductWarehouse.ToPagedList((int)page, 3));

        }

        [HttpPost]
        public ActionResult AddProductWarehouse(int Warehouse, int? page = null,string search = null)
        {
            page = (page ?? 1);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.WarehouseId =CombosHelper.GetWarehousers(user.CompanyId);
            ViewBag.IDBodega = Warehouse;
            List<Product> ListaProductWarehouse = CombosHelper.ProductWareHouse(Warehouse);

            if (!string.IsNullOrEmpty(search))
            {
                var products2 = ListaProductWarehouse.Where(s => s.Description.ToUpper().Contains(search.ToUpper())
                       || s.Stock.ToString().Contains(search)).ToList();


                return PartialView(products2.ToPagedList((int)page, 3));

            }



            return PartialView(ListaProductWarehouse.ToPagedList((int)page, 3));

        }



        [HttpPost]
        public ActionResult AddProductToList(int id, int cantidad)
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                var SaleDetailTmp = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == id).FirstOrDefault();

                if (SaleDetailTmp == null)
                {
                    var product = db.Products.Find(id);

                    SaleDetailTmp = new SaleDetailTmp
                    {
                        Description = product.Description,
                        Price = product.Price,
                        ProductId = product.ProductId,
                        Quantity = cantidad,
                        TaxRate = product.Tax.Rate,
                        UserName = User.Identity.Name,

                    };

                    db.SaleDetailTmps.Add(SaleDetailTmp);
                }
                else
                {
                    SaleDetailTmp.Quantity += cantidad;
                    db.Entry(SaleDetailTmp).State = EntityState.Modified;

                }

                db.SaveChanges();
                return Json(Url.Action("Create", "Sales"));
                //RedirectToAction("Create","Sales");
            }

            return PartialView();
        }


        [HttpPost]
        public ActionResult AddProduct(AddSaleView view)
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                var SaleDetailTmp = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == view.ProductId).FirstOrDefault();

                if (SaleDetailTmp == null)
                {
                    var product = db.Products.Find(view.ProductId);

                    SaleDetailTmp = new SaleDetailTmp
                    {
                        Description = product.Description,
                        Price = product.Price,
                        ProductId = product.ProductId,
                        Quantity = view.Quantity,
                        TaxRate = product.Tax.Rate,
                        UserName = User.Identity.Name,

                    };

                    db.SaleDetailTmps.Add(SaleDetailTmp);
                }
                else
                {
                    SaleDetailTmp.Quantity += view.Quantity;
                    db.Entry(SaleDetailTmp).State = EntityState.Modified;

                }

                db.SaveChanges();
                return RedirectToAction("Create");
            }
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId).ToList(), "ProductId", "Description");
            return PartialView(view);
        }


        // GET: Purchases
        public ActionResult AddProduct()
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId, true).ToList(), "ProductId", "Description");
            return PartialView();
        }


        // GET: Sales
        public ActionResult Index(int? page = null,string buscarCategoria = null)
        {
            page = (page ?? 1);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var sales = db.Sales.Where(o => o.CompanyId == user.CompanyId).Include(s => s.Customer).Include(s => s.State).Include(s => s.Warehouse).OrderBy(c => c.Date);

            if (!string.IsNullOrEmpty(buscarCategoria))
            {
                var sales2 = sales.Where(s => s.Warehouse.Name.ToUpper().Contains(buscarCategoria.ToUpper())
                                                    || s.Customer.FirstName.ToUpper().Contains(buscarCategoria.ToUpper())
                                                    || s.Customer.LastName.ToUpper().Contains(buscarCategoria.ToUpper())
                                                    || s.State.Description.ToUpper().Contains(buscarCategoria.ToUpper())).ToList();

                return View(sales2.ToPagedList((int)page, 5));

            }



            return View(sales.ToPagedList((int)page, 5));
        }

        // GET: Sales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var order = db.SaleDetails.Where(odt => odt.SaleId == id).ToList();
            if (order == null)
            {
                return HttpNotFound();
            }

            return View(order);
        }

        // GET: Sales/Create
        public ActionResult Create()
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(user.CompanyId),"CustomerId", "FullName");
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehousers(user.CompanyId),"WarehouseId", "Name");
            var view = new NewSaleView
            {
                Date = DateTime.Now,
                Details = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name).ToList(),

            };
            return View(view);
        }

        // POST: Sales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewSaleView view)
        {
            if (ModelState.IsValid)
            {
                var response = MovementsHelper.NewSaleView(view, User.Identity.Name);
                if (response.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(string.Empty, response.Message);
            }

            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(user.CompanyId), "CustomerId", "FullName");
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehousers(user.CompanyId), "WarehouseId", "Name");
            view.Details = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name).ToList();
            return View(view);
        }

        // GET: Sales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sale sale = db.Sales.Find(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", sale.CompanyId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "UserName", sale.CustomerId);
            //ViewBag.OrderId = new SelectList(db.Orders, "OrderId", "Remarks", sale.OrderId);
            ViewBag.StateId = new SelectList(db.States, "StateId", "Description", sale.StateId);
            ViewBag.WarehouseId = new SelectList(db.Warehouses, "WarehouseId", "Name", sale.WarehouseId);
            return PartialView(sale);
        }

        // POST: Sales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SaleId,CompanyId,CustomerId,WarehouseId,StateId,OrderId,Date,Remarks")] Sale sale)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sale).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true });
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", sale.CompanyId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "UserName", sale.CustomerId);
            //ViewBag.OrderId = new SelectList(db.Orders, "OrderId", "Remarks", sale.OrderId);
            ViewBag.StateId = new SelectList(db.States, "StateId", "Description", sale.StateId);
            ViewBag.WarehouseId = new SelectList(db.Warehouses, "WarehouseId", "Name", sale.WarehouseId);
            return PartialView(sale);
        }

        // GET: Sales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sale sale = db.Sales.Find(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            return PartialView(sale);
        }

        // POST: Sales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var sale = db.Sales.Find(id);
            db.Sales.Remove(sale);
            var response = DBHelper.SaveChanges(db);
            if (response.Succeeded)
            {
                return Json(new { success = true });
            }
            ModelState.AddModelError(string.Empty, response.Message);
            return PartialView(sale);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
