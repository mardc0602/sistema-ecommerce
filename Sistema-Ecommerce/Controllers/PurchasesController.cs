﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EComemerce_01.Models;
using EComemerce_01.Classes;
using PagedList;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;
using System.Data.SqlClient;

namespace EComemerce_01.Controllers
{
    [Authorize(Roles ="User")]
    public class PurchasesController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        [HttpPost]
        public ActionResult DeleteProduct2(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var PurchaseDetailTmp = db.PurchaseDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == id).FirstOrDefault();

            if (PurchaseDetailTmp == null)
            {
                return HttpNotFound();
            }
            db.PurchaseDetailTmps.Remove(PurchaseDetailTmp);
            db.SaveChanges();
            return Json(Url.Action("Create", "Purchases"));
        }


        public ActionResult DeleteProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var PurchaseDetailTmp = db.PurchaseDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == id).FirstOrDefault();

            if (PurchaseDetailTmp == null)
            {
                return HttpNotFound();
            }
            db.PurchaseDetailTmps.Remove(PurchaseDetailTmp);
            db.SaveChanges();
            return RedirectToAction("Create");
        }



        [HttpPost]
        public ActionResult AddProduct(AddPurchaseView view)
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                var PurchaseDetailTmp = db.PurchaseDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == view.ProductId).FirstOrDefault();
                
                if (PurchaseDetailTmp == null)
                {
                    var product = db.Products.Find(view.ProductId);
       
                    PurchaseDetailTmp = new PurchaseDetailTmp
                    {
                        Description = product.Description,
                        Cost = product.Price,
                        ProductId = product.ProductId,
                        Quantity = view.Quantity,
                        TaxRate = product.Tax.Rate,
                        UserName = User.Identity.Name,

                    };

                    db.PurchaseDetailTmps.Add(PurchaseDetailTmp);
                }
                else
                {
                    PurchaseDetailTmp.Quantity += view.Quantity;
                    db.Entry(PurchaseDetailTmp).State = EntityState.Modified;

                }

                db.SaveChanges();
                return RedirectToAction("Create");
            }
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId), "ProductId", "Description");
            return PartialView(view);
        }


        // GET: Purchases
        public ActionResult AddProduct()
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId, true), "ProductId", "Description");
            return PartialView();
        }


        [HttpPost]
        public ActionResult AddProduct2(int? page = null, string search = null)
        {
            page = (page ?? 1);
            var user = db.User
                .Where(u => u.UserName == User.Identity.Name)
                .FirstOrDefault();
            var products = db.Products
                .Include(p => p.Category)
                .Include(p => p.Tax)
                .Where(p => p.CompanyId == user.CompanyId).OrderBy(c => c.ProductId);

            if (!string.IsNullOrEmpty(search))
            {
                var products2 = products.Where(s => s.Description.ToUpper().Contains(search.ToUpper())
                       || s.Tax.Description.ToUpper().Contains(search.ToUpper())
                       || s.Category.Description.ToUpper().Contains(search.ToUpper())).ToList();


                return PartialView("AddProduct2", products2.ToPagedList((int)page, 3));

            }

            return PartialView("AddProduct2", products.ToPagedList((int)page, 3));
        }


        // GET: Purchases
        public ActionResult AddProduct2(int? page = null)
        {
            page = (page ?? 1);
            var user = db.User
                .Where(u => u.UserName == User.Identity.Name)
                .FirstOrDefault();
            var products = db.Products
                .Include(p => p.Category)
                .Include(p => p.Tax)
                .Where(p => p.CompanyId == user.CompanyId).OrderBy(c => c.ProductId);

            return PartialView(products.ToPagedList((int)page, 3));


        }


        [HttpPost]
        public ActionResult AddProductToList(int id, int cantidad)
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                var PurchaseDetailTmp = db.PurchaseDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId ==id).FirstOrDefault();

                if (PurchaseDetailTmp == null)
                {
                    var product = db.Products.Find(id);

                    PurchaseDetailTmp = new PurchaseDetailTmp
                    {
                        Description = product.Description,
                        Cost = product.Price,
                        ProductId = product.ProductId,
                        Quantity =cantidad,
                        TaxRate = product.Tax.Rate,
                        UserName = User.Identity.Name,

                    };

                    db.PurchaseDetailTmps.Add(PurchaseDetailTmp);
                }
                else
                {
                    PurchaseDetailTmp.Quantity += cantidad;
                    db.Entry(PurchaseDetailTmp).State = EntityState.Modified;

                }

                db.SaveChanges();
                return Json(Url.Action("Create","Purchases"));
                //RedirectToAction("Create","Sales");
            }

            return PartialView();
        }




        public ActionResult Index(int? page = null, string buscarCategoria = null)
        {
            page = (page ?? 1);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var purchases = db.Purchases.Where(o => o.CompanyId == user.CompanyId).Include(p => p.State).Include(p => p.Supplier).Include(p => p.Warehouse).OrderBy(c => c.Date);



            if (!string.IsNullOrEmpty(buscarCategoria))
            {
                var purchases2 = purchases.Where(s => s.Warehouse.Name.ToUpper().Contains(buscarCategoria.ToUpper())
                                                    || s.Supplier.FirstName.ToUpper().Contains(buscarCategoria.ToUpper())
                                                    || s.Supplier.LastName.ToUpper().Contains(buscarCategoria.ToUpper())
                                                    || s.State.Description.ToUpper().Contains(buscarCategoria.ToUpper())).ToList();

                return View(purchases2.ToPagedList((int)page, 5));

            }



            return View(purchases.ToPagedList((int)page, 5));
        }

        // GET: Purchases/Details/5
        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var Purchase = db.Purchases.Find(id);
            if (Purchase == null)
            {
                return HttpNotFound();
            }
            return View(Purchase);

        }
   
        public ActionResult Detallado(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var order = db.PurchaseDetails.Where(odt => odt.PurchaseId==id).ToList();
            if (order == null)
            {
                return HttpNotFound();
            }

            return View(order);

        }


        public ActionResult ImprimirDetalle(int id)
        {

            var report = this.GenerarDetalleCompra(id);
            var stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream,"application/pdf");

        }

        private ReportClass GenerarDetalleCompra(int id)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var connection = new SqlConnection(connectionString);
            var dataTable = new DataTable();

            var sql = @"SELECT Purchases.Date, PurchaseDetails.Description, PurchaseDetails.TaxRate, PurchaseDetails.Cost, PurchaseDetails.Quantity, 
                         Suppliers.FirstName, Suppliers.LastName, Suppliers.Phone, Suppliers.Adress, PurchaseDetails.PurchaseId
                        FROM            Purchases INNER JOIN
                        PurchaseDetails ON Purchases.PurchaseId = PurchaseDetails.PurchaseId INNER JOIN
                        Suppliers ON Purchases.SupplierId = Suppliers.SupplierId 
                        WHERE Purchases.PurchaseId="+id;
            try
            {
                connection.Open();
                var command = new SqlCommand(sql,connection);
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            var report = new ReportClass();
            report.FileName = Server.MapPath("/Reports/DetalleCompras.rpt");
            report.Load();
            report.SetDataSource(dataTable);
            return report;
        }


        // GET: Purchases/Create
        public ActionResult Create()
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.SupplierId = new SelectList(CombosHelper.GetSuppliers(user.CompanyId), "SupplierId", "FullName");
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehousers(user.CompanyId), "WarehouseId", "Name");
            var view = new NewPurchaseView
            {
                Date = DateTime.Now,
                Details = db.PurchaseDetailTmps.Where(odt => odt.UserName == User.Identity.Name).ToList(),

            };
            return View(view);
        }

        // POST: Purchases/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewPurchaseView view)
        {
            if (ModelState.IsValid)
            {
                var response = MovementsHelper.NewPurchase(view, User.Identity.Name);
                if (response.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(string.Empty, response.Message);
            }

            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.SupplierId = new SelectList(CombosHelper.GetSuppliers(user.CompanyId), "SupplierId", "FullName");
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehousers(user.CompanyId), "WarehouseId", "Name");
            view.Details = db.PurchaseDetailTmps.Where(odt => odt.UserName == User.Identity.Name).ToList();
            return View(view);
        }

        // GET: Purchases/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Purchase purchase = db.Purchases.Find(id);
            if (purchase == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", purchase.CompanyId);
            ViewBag.StateId = new SelectList(db.States, "StateId", "Description", purchase.StateId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "SupplierId", "UserName", purchase.SupplierId);
            ViewBag.WarehouseId = new SelectList(db.Warehouses, "WarehouseId", "Name", purchase.WarehouseId);
            return PartialView(purchase);
        }

        // POST: Purchases/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PurchaseId,CompanyId,SupplierId,StateId,Date,Remarks,WarehouseId")] Purchase purchase)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchase).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true });
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", purchase.CompanyId);
            ViewBag.StateId = new SelectList(db.States, "StateId", "Description", purchase.StateId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "SupplierId", "UserName", purchase.SupplierId);
            ViewBag.WarehouseId = new SelectList(db.Warehouses, "WarehouseId", "Name", purchase.WarehouseId);
            return PartialView(purchase);
        }

        // GET: Purchases/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Purchase purchase = db.Purchases.Find(id);
            if (purchase == null)
            {
                return HttpNotFound();
            }
            return PartialView(purchase);
        }

        // POST: Purchases/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var purchase = db.Purchases.Find(id);
            db.Purchases.Remove(purchase);
            var response = DBHelper.SaveChanges(db);
            if (response.Succeeded)
            {
                return Json(new { success = true });
            }
            ModelState.AddModelError(string.Empty, response.Message);
            return PartialView(purchase);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
