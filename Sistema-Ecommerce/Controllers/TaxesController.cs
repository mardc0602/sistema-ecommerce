﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EComemerce_01.Models;
using PagedList;
using EComemerce_01.Classes;

namespace EComemerce_01.Controllers
{
    [Authorize(Roles ="User")]
    public class TaxesController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        // GET: Taxes
        public ActionResult Index(int? page = null,string buscarCategoria = null)
        {
            page = (page ?? 1);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (user == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var taxes = db.Taxes.Where(t => t.CompanyId==user.CompanyId).OrderBy(c => c.Rate);

            if (!string.IsNullOrEmpty(buscarCategoria))
            {
                var purchases2 = taxes.Where(s => s.Description.ToUpper().Contains(buscarCategoria.ToUpper())
                                                    || s.Rate.ToString().ToUpper().Contains(buscarCategoria.ToUpper())).ToList();

                return View(purchases2.ToPagedList((int)page, 5));

            }



            return View(taxes.ToPagedList((int)page, 5));
        }

        // GET: Taxes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tax tax = db.Taxes.Find(id);
            if (tax == null)
            {
                return HttpNotFound();
            }
            return View(tax);
        }

        // GET: Taxes/Create
        public ActionResult Create()
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (user == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var tax = new Tax { CompanyId = user.CompanyId };

            return PartialView(tax);
        }

        // POST: Taxes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tax tax)
        {
            if (ModelState.IsValid)
            {
                db.Taxes.Add(tax);
                db.SaveChanges();
                return Json(new { success = true });
            }
            return PartialView(tax);
        }

        // GET: Taxes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tax = db.Taxes.Find(id);
            if (tax == null)
            {
                return HttpNotFound();
            }
            return PartialView(tax);
        }

        // POST: Taxes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tax tax)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tax).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true });
            }
            return PartialView(tax);
        }

        // GET: Taxes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tax = db.Taxes.Find(id);
            if (tax == null)
            {
                return HttpNotFound();
            }
            return PartialView(tax);
        }

        // POST: Taxes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var tax = db.Taxes.Find(id);
            db.Taxes.Remove(tax);
            var response = DBHelper.SaveChanges(db);
            if (response.Succeeded)
            {
                return Json(new { success = true });
            }
            ModelState.AddModelError(string.Empty, response.Message);
            return PartialView(tax);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
