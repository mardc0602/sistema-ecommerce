﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EComemerce_01.Models;
using EComemerce_01.Classes;
using PagedList;
using System.Web.UI.WebControls;

namespace EComemerce_01.Controllers
{
    [Authorize(Roles = "User")]
    public class OrdersController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        [HttpPost]
        public ActionResult DeleteProduct2(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var orderDetailTmp = db.OrderDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId ==id).FirstOrDefault();

            if (orderDetailTmp == null)
            {
                return HttpNotFound();
            }
            db.OrderDetailTmps.Remove(orderDetailTmp);
            db.SaveChanges();
            return Json(Url.Action("Create", "Orders"));
        }

        public ActionResult DeleteProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var orderDetailTmp = db.OrderDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == id).FirstOrDefault();

            if (orderDetailTmp == null)
            {
                return HttpNotFound();
            }
            db.OrderDetailTmps.Remove(orderDetailTmp);
            db.SaveChanges();
            return RedirectToAction("Create");
        }


        [HttpPost]
        public ActionResult AddProductToList(int id, int cantidad)
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                var orderDetailTmp = db.OrderDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == id).FirstOrDefault();
                if (orderDetailTmp == null)
                {
                    var product = db.Products.Find(id);
                    orderDetailTmp = new OrderDetailTmp
                    {
                        Description = product.Description,
                        Price = product.Price,
                        ProductId = product.ProductId,
                        Quantity = cantidad,
                        TaxRate = product.Tax.Rate,
                        UserName = User.Identity.Name,

                    };
                    db.OrderDetailTmps.Add(orderDetailTmp);
                }
                else
                {
                    orderDetailTmp.Quantity +=cantidad;
                    db.Entry(orderDetailTmp).State = EntityState.Modified;

                }

                db.SaveChanges();
                return Json(Url.Action("Create", "Orders"));
                //RedirectToAction("Create","Sales");
            }

            return PartialView();
        }


        [HttpPost]
        public ActionResult AddProduct2(int? page = null, string search = null)
        {
            page = (page ?? 1);
            var user = db.User
                .Where(u => u.UserName == User.Identity.Name)
                .FirstOrDefault();
            var products = db.Products
                .Include(p => p.Category)
                .Include(p => p.Tax)
                .Where(p => p.CompanyId == user.CompanyId).OrderBy(c => c.ProductId);

            if (!string.IsNullOrEmpty(search))
            {
                var products2 = products.Where(s => s.Description.ToUpper().Contains(search.ToUpper())
                       || s.Tax.Description.ToUpper().Contains(search.ToUpper())
                       || s.Category.Description.ToUpper().Contains(search.ToUpper())).ToList();


                return PartialView("AddProduct2", products2.ToPagedList((int)page, 3));

            }

            return PartialView("AddProduct2", products.ToPagedList((int)page, 3));
        }


        // GET: Purchases
        public ActionResult AddProduct2(int? page = null)
        {
            page = (page ?? 1);
            var user = db.User
                .Where(u => u.UserName == User.Identity.Name)
                .FirstOrDefault();
            var products = db.Products
                .Include(p => p.Category)
                .Include(p => p.Tax)
                .Where(p => p.CompanyId == user.CompanyId).OrderBy(c => c.ProductId);

            return PartialView(products.ToPagedList((int)page, 3));


        }


        [HttpPost]
        public ActionResult AddProduct(AddProductView view)
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                var orderDetailTmp = db.OrderDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == view.ProductId).FirstOrDefault();
                if (orderDetailTmp == null)
                {
                    var product = db.Products.Find(view.ProductId);
                    orderDetailTmp = new OrderDetailTmp
                    {
                        Description = product.Description,
                        Price = product.Price,
                        ProductId = product.ProductId,
                        Quantity = view.Quantity,
                        TaxRate = product.Tax.Rate,
                        UserName = User.Identity.Name,

                    };
                    db.OrderDetailTmps.Add(orderDetailTmp);
                }
                else
                {
                    orderDetailTmp.Quantity += view.Quantity;
                    db.Entry(orderDetailTmp).State = EntityState.Modified;

                }
                
                db.SaveChanges();
                return RedirectToAction("Create");
            }
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId), "ProductId", "Description");
            return PartialView(view);
        }


        public ActionResult AddProduct()
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId,true), "ProductId", "Description");
            return PartialView();
        }

        // GET: Orders
        public ActionResult Index(int? page = null,string buscarCategoria = null)
        {
            page = (page ?? 1);
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var orders = db.Orders.Where(o=>o.CompanyId==user.CompanyId).Include(o => o.Customer).Include(o => o.State).OrderBy(c => c.OrderId);

            if (!string.IsNullOrEmpty(buscarCategoria))
            {
                var ordenes2 = orders.Where(s => s.Customer.FirstName.ToUpper().Contains(buscarCategoria.ToUpper())
                                                    || s.Customer.LastName.ToUpper().Contains(buscarCategoria.ToUpper())
                                                    || s.State.Description.ToUpper().Contains(buscarCategoria.ToUpper())).ToList();

                return View(ordenes2.ToPagedList((int)page, 5));

            }

            return View(orders.ToPagedList((int)page, 5));
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var order = db.OrderDetails.Where(odt => odt.OrderId == id).ToList();
            if (order == null)
            {
                return HttpNotFound();
            }

            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(user.CompanyId), "CustomerId", "FullName");
            var view = new NewOrderView
            {
                Date = DateTime.Now,
                Details = db.OrderDetailTmps.Where(odt => odt.UserName == User.Identity.Name).ToList(),

            };
            return View(view);
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewOrderView view)
        {
            if (ModelState.IsValid)
            {
                var response = MovementsHelper.NewOrder(view,User.Identity.Name);
                if (response.Succeeded) {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(string.Empty,response.Message);
            }

            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(user.CompanyId), "CustomerId", "FullName");
            view.Details = db.OrderDetailTmps.Where(odt => odt.UserName == User.Identity.Name).ToList();
            return View(view);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(user.CompanyId), "CustomerId", "FullName");
            ViewBag.StateId = new SelectList(db.States, "StateId", "Description", order.StateId);
            return PartialView(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderId,CompanyId,CustomerId,StateId,Date,Remarks")] Order order)
        {
            
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true });

            }
            var user = db.User.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(user.CompanyId), "CustomerId", "FullName");
            ViewBag.StateId = new SelectList(db.States, "StateId", "Description", order.StateId);
            return PartialView(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return PartialView(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var order = db.Orders.Find(id);
            db.Orders.Remove(order);
            var response = DBHelper.SaveChanges(db);
            if (response.Succeeded)
            {
                return Json(new { success = true });
            }
            ModelState.AddModelError(string.Empty, response.Message);
            return PartialView(order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
