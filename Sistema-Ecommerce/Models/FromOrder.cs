﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EComemerce_01.Models
{
    public class FromOrder
    {
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Seleccionar un {0}")]
        [Display(Name = "Orden", Prompt = "[Selecciona un Orden...]")]
        public int OrderId { get; set; }


    }
}