﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EComemerce_01.Models
{
    public class Response
    {
        public bool Succeeded { get; set; }

        public string Message { get; set; }
    }
}