﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EComemerce_01.Models
{
    public class AddPurchaseView
    {
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Seleccionar un {0}")]
        [Display(Name = "Producto", Prompt = "[Seleccionar un producto...]")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Display(Name = "Cantidad")]
        [Range(0, double.MaxValue, ErrorMessage = "You must enter greather than {1} values in {0}")]

        public double Quantity { get; set; }



 

    }
}