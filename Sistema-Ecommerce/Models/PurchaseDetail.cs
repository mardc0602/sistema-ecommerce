﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EComemerce_01.Models
{
    public class PurchaseDetail
    {
        [Key]
        public int PurchaseDetailId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int PurchaseId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(100, ErrorMessage = "El campo {0} debe tener {1} caracteres de longitud como maximo")]
        [Display(Name = "Producto")]
        public string Description { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(0, double.MaxValue, ErrorMessage = "El {0} debe estar {1} y {2}")]
        [Display(Name = "Impuesto")]
        public double TaxRate { get; set; }

        [Display(Name = "Costo")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Range(0, double.MaxValue, ErrorMessage = "Ingresar {0} entre {1} y {2}")]

        public decimal Cost { get; set; }

        [Display(Name = "Cantidad")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        
        [Range(0, double.MaxValue, ErrorMessage = "Ingresar {0} entre {1} y {2}")]

        public double Quantity { get; set; }

        public virtual Purchase Purchase { get; set; }

        public virtual Product Product { get; set; }
    }
}