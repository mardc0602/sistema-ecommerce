﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EComemerce_01.Models
{
    public class CustomersView
    {
        public string Name { get; set; }

        public string Adress { get; set; }

        public List<Customer> Customers { get; set; }

    }
}