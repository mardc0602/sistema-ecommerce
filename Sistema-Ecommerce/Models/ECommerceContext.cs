﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace EComemerce_01.Models
{
    public class ECommerceContext:DbContext
    {
        public ECommerceContext():base("DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }


        public System.Data.Entity.DbSet<EComemerce_01.Models.Department> Departments { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.City> Cities { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Company> Companies { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.User> User { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Tax> Taxes { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Warehouse> Warehouses { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Inventory> Inventores { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Customer> Customers { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.State> States { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Order> Orders { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.OrderDetail> OrderDetails { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.OrderDetailTmp> OrderDetailTmps { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.CompanyCustomer> CompanyCustomers { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.CompanySupplier> CompanySuppliers { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Supplier> Suppliers { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Purchase> Purchases { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.PurchaseDetail> PurchaseDetails { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.PurchaseDetailTmp> PurchaseDetailTmps { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.Sale> Sales { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.SaleDetail> SaleDetails { get; set; }

        public System.Data.Entity.DbSet<EComemerce_01.Models.SaleDetailTmp> SaleDetailTmps { get; set; }
    }
}