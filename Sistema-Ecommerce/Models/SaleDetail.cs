﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EComemerce_01.Models
{
    public class SaleDetail
    {
        [Key]
        public int SaleDetailId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int SaleId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(100, ErrorMessage = "The filed {0} must be maximun {1} characters length")]
        [Display(Name = "Producto")]
        public string Description { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(0, double.MaxValue, ErrorMessage = "El {0} debe estar entre {1} y {2}")]
        [Display(Name = "Tax rate")]
        public double TaxRate { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Range(0, double.MaxValue, ErrorMessage = "Ingresar {0} entre {1} y {2}")]
        [Display(Name = "Precio")]

        public decimal Price { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]

        [Range(0, double.MaxValue, ErrorMessage = "Ingresar {0} entre {1} y {2}")]
        [Display(Name = "Cantidad")]

        public double Quantity { get; set; }

        public virtual Sale Sale { get; set; }

        public virtual Product Product { get; set; }
    }
}