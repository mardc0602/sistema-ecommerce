﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EComemerce_01.Startup))]
namespace EComemerce_01
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
