﻿using EComemerce_01.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EComemerce_01.Classes
{
    public class MovementsHelper : IDisposable
    {
        private static ECommerceContext db = new ECommerceContext();

        public void Dispose()
        {
            db.Dispose();
        }

        public static Response NewOrder(NewOrderView view, string userName)
        {
            using (var transacction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.User.Where(u => u.UserName == userName).FirstOrDefault();
                    var order = new Order
                    {
                        CompanyId = user.CompanyId,
                        CustomerId = view.CustomerId,
                        Date = view.Date,
                        Remarks = view.Remarks,
                        StateId = DBHelper.GetState("created", db),

                    };
                    db.Orders.Add(order);
                    db.SaveChanges();
                    var details = db.OrderDetailTmps.Where(odt => odt.UserName == userName).ToList();
                    var detallesOrden2 = db.OrderDetailTmps.Select(odt => odt.OrderDetailTmpId).First();
                    foreach (var detail in details)
                    {
                        var orderDetail = new OrderDetail
                        {
                            Description = detail.Description,
                            OrderId = order.OrderId,
                            Price = detail.Price,
                            ProductId = detail.ProductId,
                            Quantity = detail.Quantity,
                            TaxRate = detail.TaxRate
                        };
                        db.OrderDetails.Add(orderDetail);
                        db.OrderDetailTmps.Remove(detail);

                    }
                    db.SaveChanges();
                    transacction.Commit();
                    return new Response { Succeeded = true };
                }
                catch (Exception ex)
                {
                    transacction.Rollback();
                    string mensaje = "";
                    if (ex.Message=="La secuencia no contiene elementos") {
                        mensaje = "Agregar Productos";
                    }

                    return new Response
                    {
                        Message = mensaje,
                        Succeeded = false
                    };
                }
            }
        }


        public static Response NewPurchase(NewPurchaseView view, string userName)
        {
            using (var transacction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.User.Where(u => u.UserName == userName).FirstOrDefault();
                    var inventor = new Inventory();
                    var inventor2 = new Inventory();
                    List<Inventory> numeros = new List<Inventory>();
                    List<Inventory> eliminar = new List<Inventory>();

                    var purchase = new Purchase
                    {
                        CompanyId = user.CompanyId,
                        SupplierId = view.SupplierId,
                        Date = view.Date,
                        Remarks = view.Remarks,
                        StateId = DBHelper.GetState("created", db),
                        WarehouseId = view.WarehouseId

                    };
                    db.Purchases.Add(purchase);
                    db.SaveChanges();
                    var details = db.PurchaseDetailTmps.Where(odt => odt.UserName == userName).ToList();
                    var detallesOrden2 = db.PurchaseDetailTmps.Select(odt => odt.PurchaseDetailTmpId).First();

                    foreach (var detail in details)
                    {
                        var purchaseDetail = new PurchaseDetail
                        {
                            Description = detail.Description,
                            PurchaseId = purchase.PurchaseId,
                            Cost = detail.Cost,
                            ProductId = detail.ProductId,
                            Quantity = detail.Quantity,
                            TaxRate = detail.TaxRate
                        };

                        inventor = new Inventory
                        {
                            Stock = detail.Quantity,
                            ProductId = detail.ProductId,
                            WarehouseId = view.WarehouseId

                        };
                        numeros.Add(inventor);
                        db.PurchaseDetails.Add(purchaseDetail);
                        db.PurchaseDetailTmps.Remove(detail);

                    }

                    var inventarioTotal = db.Inventores.Count();
                    if (inventarioTotal==0)
                    {
                        for (int i=0;i<numeros.Count;i++) {
                            db.Inventores.Add(numeros[i]);
                        }
                    }

                    var agregados = numeros.ToArray();
                    var inventores2 = db.Inventores.ToArray();
                                        
                    for (int i = 0; i <agregados.Length; i++)
                    {
                        int contador1=0,contador2=0;
                        for (int j = 0; j < inventores2.Length; j++)
                        {

                            if (agregados[i].ProductId == inventores2[j].ProductId && agregados[i].WarehouseId == inventores2[j].WarehouseId)
                            {

                                inventor = new Inventory
                                {
                                    Stock = agregados[i].Stock + inventores2[j].Stock,
                                    ProductId = agregados[i].ProductId,
                                    WarehouseId = view.WarehouseId
                                };
                                
                                eliminar.Add(inventores2[j]);
                                eliminar.Add(agregados[i]);
                                db.Inventores.Add(inventor);
                                db.SaveChanges();
                                

                            }
                            else if (agregados[i].ProductId == inventores2[j].ProductId && agregados[i].WarehouseId!=inventores2[j].WarehouseId)
                            {                                
                                contador1++;
                                
                            }
                            else if (agregados[i].ProductId !=inventores2[j].ProductId)
                            {                  
                                contador2++;
                            }
                            
                        }

                        if (contador1== inventores2.Length||contador2>0) {
                            db.Inventores.Add(agregados[i]);
                        }
                        
                        
                    }
                    db.Inventores.RemoveRange(eliminar);
                    db.SaveChanges();
                    transacction.Commit();
                    return new Response { Succeeded = true };
                }
                catch (Exception ex)
                {
                    transacction.Rollback();
                    string mensaje = "";
                    if (ex.Message == "La secuencia no contiene elementos")
                    {
                        mensaje = "Agregar Productos";
                    }

                    return new Response
                    {
                        Message =mensaje,
                        Succeeded = false
                    };
                }
            }
        }

        public static Response NewSaleView(NewSaleView view, string userName)
        {
            using (var transacction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.User.Where(u => u.UserName == userName).FirstOrDefault();
                    var inventorA = new Inventory();
                    List<Inventory> numeros = new List<Inventory>();
                    List<SaleDetail> VentasDetalles = new List<SaleDetail>();
                    List<Inventory> eliminar = new List<Inventory>();
                    List<OrderDetail> OrderDetalleSalvar = new List<OrderDetail>();

                    var sale = new Sale
                    {
                        CompanyId = user.CompanyId,
                        CustomerId = view.CustomerId,
                        Date = view.Date,
                        Remarks = view.Remarks,
                        StateId = DBHelper.GetState("created", db),
                        WarehouseId = view.WarehouseId,

                    };
                    db.Sales.Add(sale);
                    db.SaveChanges();
                    var detalles = db.SaleDetailTmps.Where(odt => odt.UserName == userName).ToList();
                    var detallesOrden2 = db.SaleDetailTmps.Select(odt => odt.OrderId).First();
                    var orden = db.Orders.Find(detallesOrden2);
                    var ordendetalle = db.OrderDetails.Where(odt => odt.OrderId == detallesOrden2).ToArray();

                    var OrdenDetalleFacturada = new OrderDetail();

                    var OrdenFacturada = new Order();

                    if (orden != null)
                    {
                        if (detallesOrden2 == orden.OrderId)
                        {
                            OrdenFacturada = new Order()
                            {
                                OrderId = orden.OrderId,
                                CompanyId = orden.CompanyId,
                                CustomerId = orden.CustomerId,
                                StateId = DBHelper.GetState("facturada", db),
                                Date = orden.Date,
                                Remarks = orden.Remarks
                            };

                            for (int i = 0; i < ordendetalle.Length; i++)
                            {
                                db.OrderDetails.Remove(ordendetalle[i]);
                            }

                            db.Orders.Remove(orden);
                            db.Orders.Add(OrdenFacturada);
                            db.SaveChanges();


                            for (int i = 0; i < ordendetalle.Length; i++)
                            {

                                OrdenDetalleFacturada = new OrderDetail()
                                {
                                    OrderDetailId = ordendetalle[i].OrderDetailId,
                                    OrderId = OrdenFacturada.OrderId,
                                    ProductId = ordendetalle[i].ProductId,
                                    Description = ordendetalle[i].Description,
                                    TaxRate = ordendetalle[i].TaxRate,
                                    Price = ordendetalle[i].Price,
                                    Quantity = ordendetalle[i].Quantity
                                };

                                OrderDetalleSalvar.Add(OrdenDetalleFacturada);

                            }
                            db.OrderDetails.AddRange(OrderDetalleSalvar);
                            db.SaveChanges();

                        }

                    }

                    var SaleDetail = new SaleDetail();
                    foreach (var detalle in detalles)
                    {
                        SaleDetail = new SaleDetail
                        {
                            Description = detalle.Description,
                            SaleId = sale.SaleId,
                            Price = detalle.Price,
                            ProductId = detalle.ProductId,
                            Quantity = detalle.Quantity,
                            TaxRate = detalle.TaxRate
                        };

                        inventorA = new Inventory
                        {
                            Stock = detalle.Quantity,
                            ProductId = detalle.ProductId,
                            WarehouseId = view.WarehouseId

                        };
                        VentasDetalles.Add(SaleDetail);
                        numeros.Add(inventorA);
                        db.SaleDetails.Add(SaleDetail);
                        db.SaleDetailTmps.Remove(detalle);

                    }
                    
                    var agregados = numeros.ToArray();
                    var inventores2 = db.Inventores.ToArray();
                    

                    for (int i = 0; i < agregados.Length; i++)
                    {
                       int contadorProduct = 0;

                        for (int j = 0; j < inventores2.Length; j++)
                        {
                            
                            if (agregados[i].ProductId == inventores2[j].ProductId && agregados[i].WarehouseId == inventores2[j].WarehouseId)
                            {
                                contadorProduct++;
                                inventorA = new Inventory
                                {
                                    Stock = inventores2[j].Stock - agregados[i].Stock,
                                    ProductId = agregados[i].ProductId,
                                    WarehouseId = view.WarehouseId
                                };


                                if (inventorA.Stock >= 0)
                                {
                                    eliminar.Add(inventores2[j]);
                                    db.Inventores.Add(inventorA);
                                    db.SaveChanges();

                                }

                            }




                        }
                        if (contadorProduct == 0)
                        {
                            db.SaleDetails.Remove(VentasDetalles[i]);

                            
                        }
                    }

                    db.Inventores.RemoveRange(eliminar);
                    db.SaveChanges();
                    transacction.Commit();
                    return new Response { Succeeded = true };
                }
                catch (Exception ex)
                {
                    transacction.Rollback();
                    string mensaje = "";
                    if (ex.Message == "La secuencia no contiene elementos")
                    {
                        mensaje = "Agregar Productos";
                    }
                    return new Response
                    {
                        Message =mensaje,
                        Succeeded = false
                    };
                }
            }
        }
    }
}