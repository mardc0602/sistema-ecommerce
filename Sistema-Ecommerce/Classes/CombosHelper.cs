﻿using EComemerce_01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace EComemerce_01.Classes
{
    public class CombosHelper:IDisposable
    {
        private static ECommerceContext db = new ECommerceContext();
        public static List<Department>GetDepartments()
        {
            var departments = db.Departments.ToList();
            departments.Add(new Department
            {
                DepartmentId = 0,
                Name = "[Seleccionar un Departamento...]",
            });
            return departments.OrderBy(d => d.Name).ToList();

        }

        public static List<Department> GetDepartments(bool s)
        {
            var departments = db.Departments.ToList();
            return departments.OrderBy(d => d.Name).ToList();

        }


        public static List<Product> GetProducts(int companyId, bool sw)
        {
            var products = db.Products.Where(p => p.CompanyId == companyId).ToList();
            return products.OrderBy(p => p.Description).ToList();
        }

        public static List<Product> GetProducts(int companyId)
        {
            var products = db.Products.Where(p => p.CompanyId == companyId).ToList();
            products.Add(new Product
            {
              ProductId=0,
              Description = "[Seleccionar un producto...]",
            });
            return products.OrderBy(p=> p.Description).ToList();
        }

        public static List<Order> GetOrders(int companyId, int s)
        {
            var orders = db.Orders.Where(c => c.CompanyId == companyId && c.StateId==s).ToList();
            return orders.OrderBy(p => p.OrderId).ToList();
        }
        public static List<Order> GetOrders(int companyId)
        {
            var orders = db.Orders.Where(c => c.CompanyId == companyId).ToList();
            return orders.OrderBy(p => p.OrderId).ToList();
        }

        public static List<City> GetCities(int departmentId)
        {
            var cities = db.Cities.Where(c=>c.DepartmentId== departmentId).ToList();
            cities.Add(new City
            {
                CityId = 0,
                Name = "[Seleccionar una Ciudad...]",
            });
            return cities.OrderBy(d => d.Name).ToList();

        }

        public static List<City> GetCities(int departmentId,bool s)
        {
            var cities = db.Cities.Where(c => c.DepartmentId == departmentId).ToList();
            return cities.OrderBy(d => d.Name).ToList();

        }

        public static int Inventories(int ProductId,int war)
        {
            var Inventories = db.Inventores.Where(c => c.ProductId == ProductId &&c.WarehouseId==war).Select(c => c.Stock).First();
            return Convert.ToInt32(Inventories);

        }


        public static List<Product> ProductWareHouse(int war) {

            List<int> IdProductos = new List<int>();
            List<Product> Productos = new List<Product>();
            var Inventories = db.Inventores.Where(c => c.Stock >= 0 && c.WarehouseId == war).ToList();

            for (int i = 0; i < Inventories.Count(); i++)
            {
                IdProductos.Add(Inventories[i].ProductId);
            }


            for (int i = 0; i < IdProductos.Count(); i++)
            {
                int valor = IdProductos[i];

                var productos = db.Products.Where(p=>p.ProductId == valor).First();

                Productos.Add(productos);
            }

            return Productos.ToList();


        }

        public static List<Warehouse> GetWarehousers(int companyId)
        {
            var warehousers = db.Warehouses.Where(c => c.CompanyId == companyId).ToList();
            warehousers.Add(new Warehouse
            {
                WarehouseId = 0,
                Name = "[Seleccionar una Bodega...]"
            });
            return warehousers.OrderBy(d => d.Name).ToList();
        }

        public static List<Warehouse> GetWarehousers(int companyId,int WarehouseId)
        {
            var warehousers = db.Warehouses.Where(c => c.CompanyId == companyId).ToList();
            var warehousers2 = warehousers.ToArray();
            warehousers.Add(new Warehouse
            {
                WarehouseId = WarehouseId,
                Name = warehousers[0].Name 
            });
            return warehousers;
        }


        public static List<Supplier> GetSuppliers(int companyId)
        {
            var qry = (from cu in db.Suppliers
                       join cc in db.CompanySuppliers on cu.SupplierId equals cc.SupplierId
                       join co in db.Companies on cc.CompanyId equals co.CompanyId
                       where co.CompanyId == companyId
                       select new { cu }).ToList();

            var suppliers = new List<Supplier>();
            foreach (var item in qry)
            {
                suppliers.Add(item.cu);
            }

            suppliers.Add(new Supplier
            {
                SupplierId = 0,
                FirstName = "[Seleccionar un Proveedor...]",
            });
            return suppliers.OrderBy(c => c.FirstName).ThenBy(c => c.LastName).ToList();
        }

        public static List<Company> GetCompanies()
        {
            var companies = db.Companies.ToList();
            companies.Add(new Company
            {
                CompanyId = 0,
                Name = "[Seleccionar una compañia...]",
            });
            return companies.OrderBy(d => d.Name).ToList();

        }

        public void Dispose()
        {
            db.Dispose();
        }

        public static List<Category> GetCategories(int companyId)
        {
            var categories = db.Categories.Where(c=>c.CompanyId==companyId).ToList();
            categories.Add(new Category
            {
                CategoryId = 0,
                Description = "[Seleccionar una categoria...]",
            });
            return categories.OrderBy(d => d.Description).ToList();
        }

        public static List<Customer> GetCustomers(int companyId)
        {
            var qry = (from cu in db.Customers
                       join cc in db.CompanyCustomers on cu.CustomerId equals cc.CustomerId
                       join co in db.Companies on cc.CompanyId equals co.CompanyId
                       where co.CompanyId == companyId
                       select new { cu }).ToList();

            var customers = new List<Customer>();
            foreach (var item in qry) {
                customers.Add(item.cu);
            }

            customers.Add(new Customer
            {
                CustomerId = 0,
                FirstName = "[Seleccionar un cliente...]",
            });
            return customers.OrderBy(c => c.FirstName).ThenBy(c=>c.LastName).ToList();
        }


        public static List<Customer> GetCustomers(int companyId,int CustomerId2)
        {
            var qry = (from cu in db.Customers
                       join cc in db.CompanyCustomers on cu.CustomerId equals cc.CustomerId
                       join co in db.Companies on cc.CompanyId equals co.CompanyId
                       where co.CompanyId == companyId
                       select new { cu }).ToList();

            var customers = new List<Customer>();
            foreach (var item in qry)
            {
                customers.Add(item.cu);
            }
            var customers2 =db.Customers.Where(c => c.CustomerId == CustomerId2).ToArray();
            customers.Add(new Customer
            {
                CustomerId = CustomerId2,
                FirstName = customers[0].FullName,
            });
            return customers;
        }

        public static List<Tax> GetTaxes(int companyId)
        {
            var taxes = db.Taxes.Where(c => c.CompanyId == companyId).ToList();
            taxes.Add(new Tax
            {
                TaxId = 0,
                Description = "[Seleccionar un Impuesto...]",
            });
            return taxes.OrderBy(d => d.Description).ToList();
        }

        public static int GetProductsTotal(int companyId)
        {
            var products = db.Products.Where(c => c.CompanyId == companyId).Count();
            return products;
        }

        public static int GetOrdesTotal(int companyId)
        {
            var ordenes = db.Orders.Where(c=>c.CompanyId==companyId).Count();
            return ordenes;
        }

        public static int GetVentasTotal(int companyId)
        {
            var ventas = db.Sales.Where(c => c.CompanyId == companyId).Count();
            return ventas;
        }

        public static int GetComprasTotal(int companyId)
        {
            var compras = db.Purchases.Where(c => c.CompanyId == companyId).Count();
            return compras;
        }

        public static int GetUserTotal()
        {
            var usuarios = db.User.Count();
            return usuarios;
        }
        public static int GetCityTotal()
        {
            var cuidades = db.Cities.Count();
            return cuidades;
        }

        public static int GetDepartamentosTotal()
        {
            var departamentos = db.Departments.Count();
            return departamentos;
        }

        public static int GetCompañiasTotal()
        {
            var compañias = db.Companies.Count();
            return compañias;
        }

    }
}