﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EComemerce_01.Models;

namespace EComemerce_01.Classes
{
    public class DBHelper
    {
        public static Response SaveChanges(ECommerceContext db) {
            try
            {

                db.SaveChanges();
                return new Response { Succeeded = true };
            }
            catch (Exception ex)
            {
                var response = new Response { Succeeded = false };
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    response.Message = "there is a record with same value";
                }
                else if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    response.Message = "Este Registro no puede ser eliminado porque tiene registros relacionados";
                }

                else {

                    response.Message = ex.Message;
                }

                return response;

            }
        }
        public static int GetState(string description, ECommerceContext db)
        {
            var state = db.States.Where(s=> s.Description == description).FirstOrDefault();

            if (state==null) {
                state = new State { Description = description };
                db.States.Add(state);
                db.SaveChanges();
            }
            return state.StateId;
        }
    }
}